import React, {useState} from 'react';
import {nanoid} from "nanoid";
import './App.css';
import {Container, Grid} from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import IconButton from "@material-ui/core/IconButton";


const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    }
}))

const App = () => {
    const classes = useStyles()
    const [items, setItems] = useState([
        {id: nanoid(), name: 'Milk'},
        {id: nanoid(), name: 'Water bottle'},
        {id: nanoid(), name: 'Fuel'},
        {id: nanoid(), name: 'Movie ticket'}

    ]);

    const [name, setName] = useState([""]);

    const addItem = e => {
        e.preventDefault();
        setItems([
            ...items,
            {
                id: nanoid(),
                name
            }
        ]);

        setName(' ')
    };

    const removeItem = id => {
        const index = items.findIndex(i => i.id === id);
        const itemsCopy = [...items];
        itemsCopy.splice(index, 1);
        setItems(itemsCopy);
    }

    return (
        <Container maxWidth="md" className={classes.root}>
            <CssBaseline/>
            <form onSubmit={addItem}>
                <Grid container spacing={2} alignItems="center">
                    <Grid item>
                        <TextField
                            id="item name"
                            label="Item name"
                            variant="outlined"
                            value={name}
                            onChange={e => setName(e.target.value)}
                        />
                    </Grid>
                    <Grid item>
                        <Button variant="contained" color="primary" type="submit">
                            Add
                        </Button>
                    </Grid>
                </Grid>

            </form>

            <Grid container direction="column" spacing={2}>
                {items.map(item => (
                    <Grid item key={item.id}>
                        <Paper component={Box} p={2}>
                            <Grid container justify="space-between" alignItems="center">
                                <Grid item>
                                    {item.name}
                                </Grid>
                                <Grid item>
                                    <IconButton onClick={() => removeItem(item.id)}>
                                        &times;
                                    </IconButton>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>

                ))}
            </Grid>
        </Container>
    );
};

export default App;